=======================================
	MYSQL- CULMINATING ACTIVITY
=======================================

-- 1. Return the customerName of the customers who are from the Philippines

SELECT * FROM customers WHERE country = "Philippines";

-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"

SELECT contactLastName, contactFirstname FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3. Return the product name and MSRP of the product named "The Titanic"

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"

SELECT  firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5. Return the names of customers who have no registered state

SELECT customerName FROM customers WHERE state IS NULL; 

-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve

SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" && firstName = "Steve";

-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" && creditLimit > 3000;

-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'

SELECT count(orderNumber) FROM ORDERS WHERE comments LIKE "%DHL%";

-- 9. Return the product lines whose text description mentions the phrase 'state of the art'

SELECT * FROM productlines WHERE textDescription LIKE '%state of the art%';

-- 10. Return the countries of customers without duplication

SELECT DISTINCT country FROM customers;

-- 11. Return the statuses of orders without duplication

SELECT DISTINCT status FROM orders;

-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada

SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo

SELECT e.firstName, e.lastName, o.city FROM offices o JOIN employees e ON  o.officeCode = e.officeCode;

-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"

SELECT c.customerName 
FROM employees e JOIN customers c ON e.employeeNumber = c.salesRepEmployeeNumber 
WHERE e.firstName = "Leslie" && e.lastName = "Thompson";

-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"

SELECT p.productName, c.customerName 
FROM customers c JOIN orders o ON c.customerNumber = o.customerNumber 
JOIN orderdetails d ON o.orderNumber = d.orderNumber 
JOIN products p ON d.productCode = p.productCode;

-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country

 SELECT e.firstName, e.lastName, c.customerName, f.country 
 FROM payments p JOIN customers c ON p.customerNumber = c.customerNumber
 JOIN employees e ON c.salesRepEmployeeNumber = e.employeeNumber 
 JOIN offices f ON e.officeCode = f.officeCode WHERE c.country = f.country;

-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000

 SELECT p.productName, p.quantityInStock 
 FROM products p JOIN productlines l ON p.productLine = l.productLine 
 WHERE l.productline = "planes" && p.quantityInStock < 1000;

-- 18. Show the customer's name with a phone number containing "+81".

SELECT * FROM customers WHERE phone LIKE "+81%"; 

-- 19. Find all customers from US

SELECT * FROM customers WHERE country = "USA"; 

-- Show the full details of a customer named La Rochelle Gifts.

SELECT * FROM customers WHERE customerName = "La Rochelle Gifts";

=======================================
	MYSQL- STRETCH GOALS
=======================================

-- 1. Return the customer names of customers whose customer names don't have 'a' in them

SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";  

-- 2. Return the last names and first names of employees being supervised by "Anthony Bow"

SELECT e.lastName, e.firstName FROM employees e WHERE reportsTo = "1143";

-- 3. Return the product name and MSRP of the product with the highest MSRP

SELECT productName, MSRP FROM products ORDER BY MSRP DESC LIMIT 1;

-- 4. Return the number of customers in the UK

SELECT count(customerNumber) FROM customers WHERE country = "UK";

-- 5. Return the number of products per product line

SELECT count(p.productLine) FROM products p JOIN productlines l ON p.productLIne = l.productLine;

-- 6. Return the number of customers served by every employee

SELECT count(c.customerNumber) FROM customers c JOIN employees e ON c.salesRepEmployeeNumber = e.employeeNumber;

-- 7. Show the customer's name with the highest credit limit.

SELECT customerName FROM customers ORDER BY creditLimit DESC LIMIT 1;